from .userCreateView import UserCreateView
from .userDetailView import UserDetailView

from .productView import ProductList
from .productView import ProductDetail

from .verifyTokenView import VerifyTokenView