import { createRouter, createWebHashHistory } from 'vue-router'
import App from '../App.vue'
import LogIn from '../components/LogIn.vue'
import SignUp from '../components/SignUp.vue'
import Home from '../components/Home.vue'
import Account from '../components/Account.vue'
import ListProduct from '../components/ListProduct.vue'
import ProductDetail from '../components/ProductDetail.vue'
import AddProduct from '../components/AddProduct.vue'
import EditProduct from '../components/EditProduct.vue'

const routes = [
  {
    path: '/',
    name: 'root',
    component: App
  },
  {
    path: '/user/login',
    name: 'login',
    component: LogIn
  },
  {
    path: '/user/signup',
    name: 'signup',
    component: SignUp
  },
  {
    path: '/user/home',
    name: 'home',
    component: Home
  },
  {
    path: '/user/account',
    name: 'account',
    component: Account
  },
  {
    path: '/user/listproduct',
    name: 'listproduct',
    component: ListProduct
  },
  {
    path: '/user/productdetail',
    name: 'productdetail',
    props: true,
    component: ProductDetail,
  },
  {
    path: '/user/addproduct',
    name: 'addproduct',
    component: AddProduct,
  },
  {
    path: '/user/editproduct',
    name: 'editproduct',
    component: EditProduct,
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
