package com.minticc4p9grupo6.account_ms.repositories;

import com.minticc4p9grupo6.account_ms.models.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountRepository extends MongoRepository <Account,String>{ }
