package com.minticc4p9grupo6.account_ms.exceptions;

public class InsufficientBalanceException extends RuntimeException{

    public InsufficientBalanceException(String message){
        super(message);
    }
}